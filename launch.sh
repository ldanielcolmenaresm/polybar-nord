#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar

## Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done


## Launch

## Left bar
polybar arch_icon -c ~/.config/polybar/current.ini &
polybar date_time -c ~/.config/polybar/current.ini &
polybar ip -c ~/.config/polybar/current.ini &

## Right bar
polybar status_info -c ~/.config/polybar/current.ini &
polybar button -c ~/.config/polybar/current.ini &

## Center bar
polybar primary -c ~/.config/polybar/workspace.ini &

